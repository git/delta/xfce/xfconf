# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Panwar108 <caspian7pena@gmail.com>, 2019-2021
msgid ""
msgstr ""
"Project-Id-Version: Xfconf\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-20 00:48+0200\n"
"PO-Revision-Date: 2021-07-19 22:48+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Hindi (http://www.transifex.com/xfce/xfconf/language/hi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../xfconf/xfconf-cache.c:985
#, c-format
msgid "Failed to make ResetProperty DBus call"
msgstr "ResetProperty DBus हेतु अनुरोध करना विफल"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:276
#, c-format
msgid "Unable to create configuration directory \"%s\""
msgstr "\"%s\" विन्यास डायरेक्टरी बनाना विफल"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:321
#: ../xfconfd/xfconf-daemon.c:167
#, c-format
msgid "Permission denied while modifying property \"%s\" on channel \"%s\""
msgstr "चैनल \"%s\" की \"%s\" विशेषता परिवर्तन हेतु अनुमति स्वीकृत"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:386
#: ../xfconfd/xfconf-backend-perchannel-xml.c:468
#: ../xfconfd/xfconf-backend-perchannel-xml.c:633
#: ../xfconfd/xfconf-backend-perchannel-xml.c:653 ../xfconf-query/main.c:344
#, c-format
msgid "Property \"%s\" does not exist on channel \"%s\""
msgstr "विशेषता \"%s\" चैनल \"%s\" पर मौजूद नहीं है"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:600
#, c-format
msgid "Unable to remove channel \"%s\": %s"
msgstr "चैनल \"%s\" हटाना विफल : %s"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:1297
#, c-format
msgid "Invalid type for <property>: \"%s\""
msgstr "<property> हेतु अमान्य प्रकार : \"%s\""

#: ../xfconfd/xfconf-backend-perchannel-xml.c:1313
#: ../xfconfd/xfconf-backend-perchannel-xml.c:1389
#, c-format
msgid "Unable to parse value of type \"%s\" from \"%s\""
msgstr "प्रकार \"%s\" मान हेतु \"%s\" से प्राप्यता विफल"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:1369
#, c-format
msgid "The type attribute of <value> cannot be an array"
msgstr "<value> की प्रकार विशेषता श्रृंखला समूह नहीं हो सकती है"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:1378
#, c-format
msgid "Invalid type for <value>: \"%s\""
msgstr "<value> हेतु अमान्य प्रकार : \"%s\""

#: ../xfconfd/xfconf-backend-perchannel-xml.c:1654
#: ../xfconfd/xfconf-backend-perchannel-xml.c:1903
#, c-format
msgid "Channel \"%s\" does not exist"
msgstr "चैनल \"%s\" मौजूद नहीं है"

#: ../xfconfd/xfconf-backend-perchannel-xml.c:1961
#, c-format
msgid "Unable to write channel \"%s\": %s"
msgstr "चैनल \"%s\" राइट करना विफल : %s"

#: ../xfconfd/xfconf-backend-factory.c:77
#, c-format
msgid "Unable to find Xfconf backend of type \"%s\""
msgstr "\"%s\" प्रकार का Xfconf बैकएन्ड खोजना विफल"

#: ../xfconfd/xfconf-backend.c:127
#, c-format
msgid "Property names must start with a '/' character"
msgstr "विशेषता नाम का '/' अक्षर से आरंभ होना आवश्यक है"

#: ../xfconfd/xfconf-backend.c:136
#, c-format
msgid "The root element ('/') is not a valid property name"
msgstr "रुट अनुखंड ('/') विशेषता हेतु मान्य नाम नहीं है"

#: ../xfconfd/xfconf-backend.c:151
#, c-format
msgid ""
"Property names can only include the ASCII characters A-Z, a-z, 0-9, '_', "
"'-', ':', '.', ',', '[', ']', '{', '}', '<' and '>', as well as '/' as a "
"separator"
msgstr ""

#: ../xfconfd/xfconf-backend.c:160
#, c-format
msgid "Property names cannot have two or more consecutive '/' characters"
msgstr ""

#: ../xfconfd/xfconf-backend.c:171
#, c-format
msgid "Property names cannot end with a '/' character"
msgstr ""

#: ../xfconfd/xfconf-backend.c:188
#, c-format
msgid "Channel name cannot be an empty string"
msgstr ""

#: ../xfconfd/xfconf-backend.c:203
#, c-format
msgid ""
"Channel names can only include the ASCII characters A-Z, a-z, 0-9, '{', '}',"
" '|', ']', '[', ':', ',', '.', '_', and '-'"
msgstr ""

#: ../xfconfd/xfconf-backend.c:425
#, c-format
msgid ""
"The property name can only be empty or \"/\" if a recursive reset was "
"specified"
msgstr ""

#: ../xfconfd/main.c:127
#, c-format
msgid "Name %s lost on the message dbus, exiting."
msgstr "संदेश में नाम %s अनुपस्थित, प्रक्रिया निरस्त हो रही है।"

#: ../xfconfd/main.c:151
msgid "Prints the xfconfd version."
msgstr ""

#: ../xfconfd/main.c:153
msgid ""
"Configuration backends to use.  The first backend specified is opened "
"read/write; the others, read-only."
msgstr ""

#: ../xfconfd/main.c:156
msgid "Fork into background after starting; only useful for testing purposes"
msgstr ""

#: ../xfconfd/main.c:176
msgid "Xfce Configuration Daemon"
msgstr "एक्सएफसीई विन्यास प्रोग्राम"

#: ../xfconfd/main.c:181
msgid "Xfce configuration daemon"
msgstr "एक्सएफसीई विन्यास प्रोग्राम"

#: ../xfconfd/main.c:183
msgid "Report bugs to http://bugs.xfce.org/\n"
msgstr "समस्याएँ आदि http://bugs.xfce.org/ पर दर्ज करें\n"

#: ../xfconfd/main.c:186
#, c-format
msgid "Error parsing options: %s\n"
msgstr "विकल्प प्राप्यता हेतु त्रुटि : %s\n"

#: ../xfconfd/xfconf-daemon.c:222
#, c-format
msgid "GType transformation failed \"%s\""
msgstr "जीटाइप रूपांतरण विफल \"%s\""

#: ../xfconfd/xfconf-daemon.c:483
#, c-format
msgid "No backends could be started"
msgstr "बैकएन्ड आरंभ विफल"

#: ../xfconf-query/main.c:90 ../xfconf-query/main.c:95
msgid "set"
msgstr "सेट"

#: ../xfconf-query/main.c:100
msgid "reset"
msgstr "पुनः सेट"

#: ../xfconf-query/main.c:176
msgid "Version information"
msgstr "संस्करण सूचना"

#: ../xfconf-query/main.c:180
msgid "The channel to query/modify"
msgstr "खोज/परिवर्तन हेतु चैनल"

#: ../xfconf-query/main.c:184
msgid "The property to query/modify"
msgstr "खोज/परिवर्तन हेतु विशेषता"

#: ../xfconf-query/main.c:188
msgid "The new value to set for the property"
msgstr ""

#: ../xfconf-query/main.c:192
msgid "List properties (or channels if -c is not specified)"
msgstr ""

#: ../xfconf-query/main.c:196
msgid "Print property and value in combination with -l or -m"
msgstr ""

#: ../xfconf-query/main.c:200
msgid "Create a new property if it does not already exist"
msgstr ""

#: ../xfconf-query/main.c:204
msgid "Specify the property value type"
msgstr ""

#: ../xfconf-query/main.c:208
msgid "Reset property"
msgstr "विशेषता पुनः सेट करें"

#: ../xfconf-query/main.c:212
msgid "Recursive (use with -r)"
msgstr ""

#: ../xfconf-query/main.c:216
msgid "Force array even if only one element"
msgstr ""

#: ../xfconf-query/main.c:220
msgid "Invert an existing boolean property"
msgstr ""

#: ../xfconf-query/main.c:224
msgid "Monitor a channel for property changes"
msgstr ""

#: ../xfconf-query/main.c:243
msgid "- Xfconf commandline utility"
msgstr "- Xfconf कमांड लाइन साधन"

#: ../xfconf-query/main.c:248
#, c-format
msgid "Option parsing failed: %s"
msgstr "विकल्प प्राप्यता विफल : %s"

#: ../xfconf-query/main.c:258
msgid "The Xfce development team. All rights reserved."
msgstr "एक्सएफसीई विकासकर्ता टीम। सर्वाधिकार सुरक्षित।"

#: ../xfconf-query/main.c:259
#, c-format
msgid "Please report bugs to <%s>."
msgstr "समस्याएँ आदि <%s> पर दर्ज करें।"

#: ../xfconf-query/main.c:268
msgid "No property specified"
msgstr "कोई विशेषता निर्दिष्ट नहीं"

#: ../xfconf-query/main.c:274
msgid "--create and --reset options can not be used together"
msgstr ""

#: ../xfconf-query/main.c:280
msgid "--create and --reset options can not be used together with --list"
msgstr ""

#: ../xfconf-query/main.c:286
#, c-format
msgid "Failed to init libxfconf: %s"
msgstr ""

#: ../xfconf-query/main.c:297
msgid "Channels:"
msgstr "चैनल :"

#: ../xfconf-query/main.c:322
#, c-format
msgid "Start monitoring channel \"%s\":"
msgstr ""

#: ../xfconf-query/main.c:360 ../xfconf-query/main.c:471
#: ../xfconf-query/main.c:544
msgid "Failed to set property"
msgstr "विशेषता सेट करना विफल"

#: ../xfconf-query/main.c:364
msgid "--toggle only works with boolean values"
msgstr ""

#: ../xfconf-query/main.c:373 ../xfconf-query/main.c:392
msgid "(unknown)"
msgstr "(अज्ञात)"

#: ../xfconf-query/main.c:382
#, c-format
msgid "Value is an array with %d items:"
msgstr ""

#: ../xfconf-query/main.c:406
#, c-format
msgid ""
"Property \"%s\" does not exist on channel \"%s\". If a new property should "
"be created, use the --create option"
msgstr ""

#: ../xfconf-query/main.c:415
msgid "When creating a new property, the value type must be specified"
msgstr ""

#: ../xfconf-query/main.c:426
msgid "Failed to get the existing type for the value"
msgstr "मान हेतु मौजूदा प्रकार की प्राप्ति विफल"

#: ../xfconf-query/main.c:445
msgid "Unable to determine the type of the value"
msgstr ""

#: ../xfconf-query/main.c:452
msgid "A value type must be specified to change an array into a single value"
msgstr ""

#: ../xfconf-query/main.c:463 ../xfconf-query/main.c:528
#, c-format
msgid "Unable to convert \"%s\" to type \"%s\""
msgstr ""

#: ../xfconf-query/main.c:497
#, c-format
msgid "There are %d new values, but only %d types could be determined"
msgstr ""

#: ../xfconf-query/main.c:519
#, c-format
msgid "Unable to determine type of value at index %d"
msgstr ""

#: ../xfconf-query/main.c:572
#, c-format
msgid "Channel \"%s\" contains no properties"
msgstr ""
